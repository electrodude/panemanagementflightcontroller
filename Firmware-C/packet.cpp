#define PACKET_DEBUG 0

#include <cstdlib>
#if PACKET_DEBUG
#include <cstdio>
#endif

#include "packet.hpp"

#define SOH 0x01
#define ESC 0x1b

#define PACKET_CRC_POLYNOMIAL 0xed2f

// checksum

void packet_checksum::putc(uint8_t c)
{
	acc ^= c;

	for (int i = 0; i < 8; i++)
	{
		acc = (acc >> 1) ^ (acc & 1 ? PACKET_CRC_POLYNOMIAL : 0);
	}
}


// new packet
void packet_out::newpkt(uint16_t len)
{
	this->char_out(SOH);

	this->crc.reset();

	this->set_remaining(2);

	this->putw(len);

	this->set_remaining(len);
}

// send byte
void packet_out::putc(uint8_t c)
{
	if (this->state != packet_out::DATA) return;

	if (c == SOH || c == ESC)
	{
		this->char_out(ESC);
		this->char_out(c ^ 0x20);
	}
	else
	{
		this->char_out(c);
	}

	this->crc.putc(c);

	if (this->remaining > 0)
	{
		this->remaining--;

		if (this->remaining == 0 && this->state == packet_out::DATA)
		{
			this->state = packet_out::END;
		}
	}
}

// send word
void packet_out::putw(uint16_t w)
{
	this->putc((w      ) & 0xFF);
	this->putc((w >>  8) & 0xFF);
}

// send long
void packet_out::putl(uint32_t l)
{
	this->putc((l      ) & 0xFF);
	this->putc((l >>  8) & 0xFF);
	this->putc((l >> 16) & 0xFF);
	this->putc((l >> 24) & 0xFF);
}


bool packet_out::putcrc()
{
	if (this->state != packet_out::END)
	{
		this->state = packet_out::FAIL;

		this->char_out(SOH);

		this->char_out_cancel();

#if PACKET_DEBUG
		printf("packet canceled\n");
#endif

		return false;
	}

	this->set_remaining(2);

	this->putw(this->crc.get());

	this->char_out_flush();

#if PACKET_DEBUG
	printf("packet sent\n");
#endif

	return true;
}


// send packet
void packet_out::send(uint16_t len, uint8_t* data)
{
	this->newpkt(len);

	for (uint16_t i = 0; i < len; i++)
	{
		this->putc(data[i]);
	}

	this->putcrc();
}



// receive unescaped byte
int packet_in::getc_raw()
{
	int c = this->char_in();

	if (c < 0)
	{
#if PACKET_DEBUG
		printf("char_in failed\n");
#endif
		this->state = packet_in::FAIL;

		return c;
	}

	if (c == SOH)
	{
#if PACKET_DEBUG
		printf("Unexpected SOH\n");
#endif
		this->state = packet_in::START;
		return -SOH;
	}

	return c;
}

// receive byte
int packet_in::getc()
{
	if (this->state != packet_in::DATA) return -1;

#if PACKET_DEBUG
	printf("getc: state = %s, remaining = %d\n", this->get_state_str(), this->remaining);
#endif

	int c = this->getc_raw();

	if (c < 0) return c;

	if (c == ESC)
	{
		int c2 = this->getc_raw();

		if (c2 < 0) return c2;

		c = c2 ^ 0x20;
		if (c != SOH && c != ESC)
		{
#if PACKET_DEBUG
			printf("Invalid ESC %02x\n", c2);
#endif

			this->state = packet_in::FAIL;

			return -ESC;
		}
	}


#if PACKET_DEBUG
	printf("getc: %02x\n", c);
#endif

	this->crc.putc(c);

	if (this->remaining > 0)
	{
		this->remaining--;

		if (this->remaining == 0 && this->state == packet_in::DATA)
		{
			this->state = packet_in::END;
		}
	}

	return c;
}

// receive word
int packet_in::getw()
{
	int c0 = this->getc();
	if (c0 < 0) return c0;

	int c1 = this->getc();
	if (c1 < 0) return c1;

	return (c1 <<  8) |
	       (c0      );
}

// receive long
int packet_in::getl()
{
	int c0 = this->getc();
	if (c0 < 0) return c0;

	int c1 = this->getc();
	if (c1 < 0) return c1;

	int c2 = this->getc();
	if (c2 < 0) return c2;

	int c3 = this->getc();
	if (c3 < 0) return c3;

	return (c3 << 24) |
	       (c2 << 16) |
	       (c1 <<  8) |
	       (c0      );
}

// receive checksum
bool packet_in::getcrc()
{
#if PACKET_DEBUG
	printf("getcrc\n");
#endif

	while (this->state == packet_in::DATA) this->getc();

	if (this->state != packet_in::END) return false;

	this->set_remaining(2);

	uint16_t crc_expect = this->crc.get();

	uint16_t crc_actual = this->getw();

	bool ok = crc_actual == crc_expect;

	this->state = ok ? packet_in::END : packet_in::FAIL;

	return ok;
}


const char* packet_in::get_state_str() const
{
	switch(this->state)
	{
		case packet_in::START: return "START";
		case packet_in::DATA: return "DATA";
		case packet_in::END: return "END";
		case packet_in::FAIL: return "FAIL";
	}

	return "???";
}



// receive packet
void packet_in::recv()
{
	if (this->state != packet_in::START)
	{
		int c;
		do
		{
			c = this->char_in();
			if (c < 0) return;
		} while (c != SOH);
	}

#if PACKET_DEBUG
	printf("Packet start\n");
#endif

	this->crc.reset();

	this->set_remaining(2);

	int len_s = this->getw();

	if (len_s < 0)
	{
#if PACKET_DEBUG
		printf("Invalid length: -%x\n", -len_s);
#endif
		// Don't do this; it's already done by this->getw() if len_s < 0
		//this->state = packet_in::FAIL;
	}
	else
	{
		uint16_t len = len_s;

#if PACKET_DEBUG
		printf("Length: %x\n", len);
#endif

		this->set_remaining(len);

		this->recv_pkt(len);
	}
}
