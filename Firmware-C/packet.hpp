#ifndef PACKET_HPP
#define PACKET_HPP

#include <cstdint>

class packet_checksum
{
public:
	void putc(uint8_t c);

	void reset() { acc = 0; }

	uint16_t get() const { return this->acc; }
private:
	uint16_t acc;
};

class packet_out
{
public:
	void send(uint16_t len, uint8_t* data);

	void newpkt(uint16_t len);

	void putc(uint8_t c);
	void putw(uint16_t w);
	void putl(uint32_t l);
	bool putcrc();

protected:
	virtual void char_out(uint8_t c) = 0;
	virtual void char_out_flush() {}
	virtual void char_out_cancel() { char_out_flush(); }

private:
	void set_remaining(uint16_t remaining) { this->remaining = remaining; this->state = remaining ? packet_out::DATA : packet_out::END; }

	enum state_t
	{
		START,
		DATA,
		END,
		FAIL,
	} state;

	uint16_t remaining;

	packet_checksum crc;
};

class packet_in
{
public:
	packet_in() : state(packet_in::END), remaining(0) {}

	void recv();

protected:
	virtual void recv_pkt(uint16_t len) = 0;

	virtual int char_in() = 0;

	int getc();
	int getw();
	int getl();
	bool getcrc();

	uint16_t get_remaining() const { return this->remaining; }
	bool ok() const { return this->state == packet_in::DATA; }
	const char* get_state_str() const;
private:
	void set_remaining(uint16_t remaining) { this->remaining = remaining; this->state = remaining ? packet_in::DATA : packet_in::END; }

	enum state_t
	{
		START,
		DATA,
		END,
		FAIL,
	} state;

	int getc_raw();

	uint16_t remaining;

	packet_checksum crc;
};

#endif
