#ifndef PACKET_PROP_HPP
#define PACKET_PROP_HPP

#include "serial_4x.h"

#include "packet.hpp"


#define PACKET_SERIAL_4X_TIMEOUT_DEFAULT 50


class packet_out_serial_4x : public packet_out
{
public:
	packet_out_serial_4x(int _port) : port(_port) {}

protected:
	virtual void char_out(uint8_t c)
	{
		S4_Put(this->port, c);
	}

	int port;
};

class packet_in_serial_4x : public packet_in
{
public:
	packet_in_serial_4x(int _port) : port(_port), timeout(PACKET_SERIAL_4X_TIMEOUT_DEFAULT) {}
	packet_in_serial_4x(int _port, int _timeout) : port(_port), timeout(_timeout) {}

protected:
	virtual int char_in()
	{
		return S4_Get_Timed(this->port, this->timeout);
	}

	int port;
	int timeout;
};

#endif
