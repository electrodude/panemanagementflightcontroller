#include <propeller.h>

#include "serial_4x.h"

#include "packet_serial_4x.hpp"

#define S4_PACKET_PORT 0


void set_pins(unsigned int start, unsigned int n, uint32_t x)
{
	uint32_t mask = (0xFFFFFFFF >> (32 - n)) << start;

	DIRA |= mask;

	OUTA = (OUTA & ~mask) | ((x << start) & mask);
}

int received = 0;
int nfail = 0;


class packet_in_serial_4x_test : public packet_in_serial_4x
{
public:
	packet_in_serial_4x_test(packet_out_serial_4x& _out, int _port) : packet_in_serial_4x(_port), out(_out) {}
	packet_in_serial_4x_test(packet_out_serial_4x& _out, int _port, int _timeout) : packet_in_serial_4x(_port, _timeout), out(_out) {}

protected:
	virtual void recv_pkt(uint16_t len)
	{
		int first = 0;

		if (len)
		{
			first = this->getc();
			if (first < 0) goto fail;

			for (unsigned int i = 1; i < len; i++)
			{
				int c2 = this->getc();
				if (c2 < 0) goto fail;

				bool correct = c2 == ((first + i) & 0xFF);
				if (!correct) goto fail;
			}
		}

		received++;

		if (!this->getcrc()) // if the received packet was wrong
		{
			goto fail;
		}

		set_pins(16, 4, first >> 4);

		this->out.newpkt(len);

		for (unsigned int i = 0; i < len; i++)
		{
			this->out.putc((first + i) & 0xFF);
		}

		this->out.putcrc();

		return;

fail:
		this->out.newpkt(1);
		this->out.putcrc();

		nfail++;
		set_pins(20, 4, nfail);
		return;
	}

	packet_out_serial_4x& out;
};

static char tx_buf[64];
static char rx_buf[64];

int main()
{
	S4_Initialize();
	S4_Define_Port(S4_PACKET_PORT, 115200, 30, tx_buf, sizeof(tx_buf), 31, rx_buf, sizeof(rx_buf));
	S4_Start();

	packet_out_serial_4x out(S4_PACKET_PORT);
	packet_in_serial_4x_test in(out, S4_PACKET_PORT);

	set_pins(16, 8, 0);

	while (1)
	{
		in.recv();

		//S4_Put(S4_PACKET_PORT, S4_Get(S4_PACKET_PORT));

		//set_pins(22, 1, CNT >> 25);

		//set_pins(16, 7, received);
	}
}
